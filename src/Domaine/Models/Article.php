<?php
/**
 * Created by PhpStorm.
 * User: issam
 * Date: 19/07/19
 * Time: 16:43
 */

namespace App\Domaine\Models;


class Article {

    /**
     * @var string
     */
    private $content;


    /**
     * Article constructor
     *
     * @param string $content
     */
    public function __construct(string $content){

        $this->content = $content;

    }



    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }


} 