<?php
/**
 * Created by PhpStorm.
 * User: issam
 * Date: 19/07/19
 * Time: 16:49
 */

namespace App\Tests\Domaine\Models;

use App\Domaine\Models\Article;
use PHPUnit\Framework\TestCase;

class ArticleTest  extends TestCase{


    public function testConstructor(){

        $article = new Article('toto');

        static::assertSame('toto', $article->getContent() );
    }

    public function testConstructorWithEmptyString(){

        $article = new Article('');

        static::assertSame('' , $article->getContent());
    }

    /*public function testConstructorWithOtherType(){

        $article = new Article(1);

        static::assertSame(1 , $article->getContent());
    }*/


} 